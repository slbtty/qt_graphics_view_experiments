#include <QApplication>
#include "myview.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    auto v = new myview();
    v->show();

    return QApplication::exec();
}
