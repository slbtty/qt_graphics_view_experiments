#ifndef MYVIEW_H
#define MYVIEW_H

#include <QGraphicsView>

class myview : public QGraphicsView
{
    Q_OBJECT
public:
    myview();

private:
    QGraphicsScene * scene;

    QPoint dragBegin;
    QPoint dragEnd;

    QGraphicsPolygonItem * selection;

    void mouseMoveEvent(QMouseEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;

    void mouseReleaseEvent(QMouseEvent *e) override;
};

#endif // MYVIEW_H
